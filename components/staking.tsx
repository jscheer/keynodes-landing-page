import { FADE_IN_ANIMATION_SETTINGS } from "@/lib/constants";
import { AnimatePresence, motion } from "framer-motion";
import Image from "next/image";
import Link from "next/link";
import { ReactNode } from "react";
import useScroll from "@/lib/hooks/use-scroll";
import Balancer from "react-wrap-balancer";
import { DEPLOY_URL, FADE_DOWN_ANIMATION_VARIANTS } from "@/lib/constants";

export default function Staking( ) {

  return (
    <motion.div
      className="w-full px-5 xl:px-0"
      initial="hidden"
      whileInView="show"
      animate="show"
      viewport={{ once: true }}
      variants={{
        hidden: {},
        show: {
          transition: {
            staggerChildren: 0.15,
          },
        },
      }}
    >

    <motion.h1
      className="bg-gradient-to-br from-black to-stone-500 bg-clip-text text-center font-display text-4xl font-bold tracking-[-0.02em] text-transparent drop-shadow-sm md:text-7xl md:leading-[5rem]"
      variants={FADE_DOWN_ANIMATION_VARIANTS}
    >
      <Balancer>What is Staking?</Balancer>
    </motion.h1>

    <motion.p
      className="mt-6 text-center text-gray-500 md:text-xl"
      variants={FADE_DOWN_ANIMATION_VARIANTS}
    >
      <Balancer>
        Support ????? by running ??????an Ethereum Full Node and provide
        SSV Staking adawd dawddad  adwawda Support ????? by running ??????an Ethereum Full Node and provide
        SSV Staking adawd dawddad
      </Balancer>
    </motion.p>

    </motion.div>
  );
}
