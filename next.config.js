/** @type {import('next').NextConfig} */

const { i18n } = require('./next-i18next.config')

const nextConfig = {
  i18n,
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ["lh3.googleusercontent.com"],
  },
  async redirects() {
    return [
      {
        source: "/gitlab",
        destination: "https://gitlab.com/jscheer/keynodes-landing-page",
        permanent: false,
      },
    ];
  },
};

module.exports = nextConfig;
